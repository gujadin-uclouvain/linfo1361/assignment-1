"""
Name of the author(s):
- Louis Navarre <louis.navarre@uclouvain.be>
"""
import time
import sys
from search import *


#################
# Problem class #
#################
from search import best_first_graph_search


class Rubik2D(Problem):
    def actions(self, state):
        """Return the actions that can be executed in the given
        state. The result would typically be a list, but if there are
        many actions, consider yielding them one at a time in an
        iterator, rather than building them all at once."""
        # STRUCTURE => ( V|H,
        #                if V: Cols_n# else Row_n#,
        #                Nbr_Move
        #              )
        nbr_rows, nbr_cols = state.shape[0], state.shape[1]
        lactions = []                                               # list of actions

        for axe in ('H', 'V'):                                      # Loop on rows and columns
            if axe == 'H':                                          # Check the working axe
                for direction in range(nbr_rows):                   # Loop on all rows
                    for nbr_mv in range(1, nbr_cols):               # Loop on all possible movements (in one direction)
                        lactions.append((axe, direction, nbr_mv))   # Add tuple of action in lactions

            else:                                                   # Check the working column
                for direction in range(nbr_cols):                   # Loop on all columns
                    for nbr_mv in range(1, nbr_rows):               # Loop on all possible movements (in one direction)
                        lactions.append((axe, direction, nbr_mv))   # Add tuple of action in lactions

        return lactions

    def result(self, state, action):
        """Return the state that results from executing the given
            action in the given state. The action must be one of
            self.actions(state)."""
        if action[0] == 'H': # Working on rows
            # New state which modify the grid and the movement name
            return State(state.shape, self.__set_new_grid(state.grid, True, action[1], action[2]), state.answer, self.__format_move_desc(True, action))
        else:                # Working on columns (V)
            # New state which modify the grid and the movement name
            return State(state.shape, self.__set_new_grid(state.grid, False, action[1], action[2]), state.answer, self.__format_move_desc(False, action))

    def goal_test(self, state):
        """Return True if the state is a goal. The default method compares the
            state to self.goal or checks for state in self.goal if it is a
            list, as specified in the constructor. Override this method if
            checking against a single self.goal is not enough."""
        for row in range(len(state.grid)):
            for col in range(len(state.grid[0])):
                if state.grid[row][col] != state.answer[row][col]:
                    return False
        return True

    def __set_new_grid(self, grid, is_row, which, how_many):
        """Return a new grid with the correct row/column modified"""
        new_grid = []
        if is_row:                                                          # Need to slide a row
            for which_row, row in enumerate(grid):                          # Recreate a new grid
                if which_row == which:                                      # Row which needs to be slide
                    new_grid.append(self.__slide_left_up(row, how_many))
                else:                                                       # Others Rows
                    new_grid.append(row)

        else: #is_col                                                       # Need to slide a column
            new_col = []
            for which_col in range(len(grid)):
                # Put all the elements from the correspond column into a list
                new_col.append(grid[which_col][which])
            new_col = tuple(self.__slide_left_up(new_col, how_many)) # Slide the list and transform it in a tuple

            for nbr_row, row in enumerate(grid):                            # Recreate a new grid
                new_grid.append(row[:which] + tuple(new_col[nbr_row]) + row[which+1:])

        return tuple(new_grid)

    def __slide_left_up(self, row, how_many):
        """Simulate a slide from left or up"""
        return row[how_many:] + row[0:how_many]

    def __format_move_desc(self, is_row, action):
        """Set the correct movement name depending on the action direction"""
        if is_row:
            return "Row #{} left {}".format(action[1], action[2])
        else:
            return "Col #{} up {}".format(action[1], action[2])


###############
# State class #
###############
class State:

    def __init__(self, shape, grid, answer=None, move="Init"):
        self.shape = shape
        self.answer = answer
        self.grid = grid
        self.move = move

    def __str__(self):
        s = self.move + "\n"
        for line in self.grid:
            s += "".join(line) + "\n"
        return s

    def __hash__(self):
        """Make a hash only based on the gird"""
        return hash(self.grid)

    def __eq__(self, other):
        """Compare states only based on grid"""
        for i in range(len(self.grid)):
            for j in range(len(self.grid[0])):
                if self.grid[i][j] != other.grid[i][j]:
                    return False
        return True


def read_instance_file(filepath):
    with open(filepath) as fd:
        lines = fd.read().splitlines()

    shape_x, shape_y = tuple([int(i) for i in lines[0].split(" ")])
    initial_grid = list()
    for row in lines[1:1 + shape_x]:
        initial_grid.append(tuple([i for i in row]))

    goal_grid = list()
    for row in lines[1 + shape_x + 1:]:
        goal_grid.append(tuple([i for i in row]))

    return (shape_x, shape_y), initial_grid, goal_grid


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(f"Usage: ./rubik2D.py <path_to_instance_file>")
    filepath = sys.argv[1]

    shape, initial_grid, goal_grid = read_instance_file(filepath)

    init_state = State(shape, tuple(initial_grid), tuple(goal_grid), "Init")
    problem = Rubik2D(init_state)

    # Example of search
    start_timer = time.perf_counter()
    node, nb_explored, remaining_nodes = breadth_first_tree_search(problem)
    end_timer = time.perf_counter()

    # Example of print
    path = node.path()

    for n in path:
        # assuming that the __str__ function of state outputs the correct format
        print(n.state)

    print("* Execution time:\t", str(end_timer - start_timer))
    print("* Path cost to goal:\t", node.depth, "moves")
    print("* #Nodes explored:\t", nb_explored)
    print("* Queue size at goal:\t",  remaining_nodes)
