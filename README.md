[LINFO1361] IA - Assignment n°1
---

**Membres:**

* Guillaume Jadin
* Thomas Robert


**Description:**

* Upload your best 2D Rubik's square solver ! It must solve the problem to optimality, that is reaching the goal state with a minimum number of moves.

* Your file must be named rubik2D.py and satisfy the output format specified in the assignment description. Otherwise, an "internal error" message is displayed by INGInious.

* Your solver will be tested against 15 instances: 10 instances are available to you with the assignment, the remaining 5 are hidden instances.

* For each instance, your solver will have a maximum of 45 seconds in order to solve it to optimality. If the time limit is exceeded, or if the solution is not optimal, the instance is failed.


**Restrictions:**

* rubik2D.py (INGInious): The file containing your implementation of the 2D Rubik’s square solver. Your program should take one argument, the filepath containing the instance to solve. It should print a solution to the problem on the standard output, respecting the format described further. The file must be encoded in utf-8.

* report_A1_group_XX.pdf (Gradescope): Answers to all the questions using the provided template. Remember, the more concise the answers, the better.


**INGInious:**

* https://inginious.info.ucl.ac.be/course/LINGI2261/2022_A1_knight


**Overleaf:**

* https://www.overleaf.com/4613823811nyykgsbkktrv